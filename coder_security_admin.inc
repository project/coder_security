<?php
/**
 * Admin form
 */
function coder_security_admin_settings() {
  $form = array();
  $form['coder_security'] = array(
    '#type' => 'fieldset',
    '#title' => t('Coder Security settings'),
  );

  $form['coder_security']['coder_security_mailto_settings'] = array(
    '#type' => 'textarea',
    '#title' => t('To who send mails when find security errors'),
    '#default_value' => variable_get('coder_security_settings', variable_get('site_mail', NULL)),
    '#descrition' => t('Enter mails seperated by commas.'),
    '#required' => TRUE,
  );
  
  $form['coder_security']['coder_security_error_messages'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter the message you wish to send when finding errors'),
    '#default_value' => variable_get('coder_security_error_messages', t('Found SQL security code errors on site: !site', array('!site'=>$site_name))),
    '#description' => t('Suspected modules are attached automatically to the message'),
    '#required' => TRUE,
  );
  
  $form['coder_security']['coder_security_linux_log'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log errors to Linux log?'),
    '#default_value' => variable_get('coder_security_linux_log', FALSE),
    '#description' => t('If checked, a general error will be logged into your linux machine.<br /> This is good, if your linux machine is being monitored.'),
  );
  
  $form['coder_security']['positive_falses'] = array(
  	'#type' => 'textarea',
  	'#title' => t('A list of known false positives modules'),
  	'#default_value' => variable_get('positive_falses', NULL),
  	'#description' => t('List of modules, <u>one module per line</u>, of all known modules with false positive, <u>the test will skip these modules.</u> <b>Handle with care !!</b>'),
  );

  return system_settings_form($form);
}
/**
 * Menu callback for admin/settings/coder_security/run
 */
function coder_security_admin_run() {
  coder_security_run();
  
  $errors = variable_get('coder_security_error', NULL);
  
  if (!empty($errors)) {
    
    //Prepare data for table
    $rows = array();
    $cells = array();
    foreach ($errors as $module => $file) {
      
      $rows['data']['module'] = $module;
      $rows['data']['file'] = $file;
      $cells[] = $rows;
    }
    $cells[] = $rows;
    $headers = array('Module', 'File');
    $output = '';
    $output .= theme('table', $headers, $cells);
    $output .= t('<div class="error">Please run <a href="@url">Coder</a> to find specific locations<br /> and fix the errors.</div>', array('@url' => '/coder'));
    return $output;
  }
  else {
    return t('Security test has run and returned no errors.');
  }
}
